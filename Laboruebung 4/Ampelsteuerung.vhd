library ieee;
use ieee.std_logic_1164.all;

entity Ampelschaltung is
	port (
		ClkRight, Mealy, Moore: in std_logic;
		Auto: out std_logic_vector (2 downto 0);
		Fusganger: out std_logic_vector (1 downto 0);
		R, R1: out std_logic
	);
end entity Ampelschaltung;

architecture Ampel of Ampelschaltung is
begin
	ampe: process (ClkRight, Mealy, Moore) is
		type int20 is range 0 to 19;
		variable counter: int20;
		variable activated: int20;
	begin
		if (ClkRight = '1' and ClkRight'event) then
			if (Mealy = '1' or Moore = '1') then
				if (Moore = '1') then
					if (counter = 1) then
						Auto <= "010";
						Fusganger <= "10";
					end if;
				end if;
				if (counter = 2) then
					Auto <= "010";
					Fusganger <= "10";
				end if;
				if (counter = 3) then
					Auto <= "100";
					Fusganger <= "10";
				end if;
				if (counter = 5) then
					Auto <= "100";
					Fusganger <= "01";
				end if;
				if (counter = 10) then
					R <= '1';
					Auto <= "100";
					Fusganger <= "10";
				end if;
				if (counter = 12) then
					Auto <= "010";
					Fusganger <= "10";
				end if;
				if (counter = 14) then
					Auto <= "001";
					Fusganger <= "10";
					R1 <= '1';
					activated := 0;
				end if;
				counter := counter + 1;
			else
				R <= '1';
				R1 <= '1';
			end if;
		end if;
		
		if (counter = 11) then
			R <= '0';
		end if;
		
		if (Moore = '0' and Mealy = '0' and activated = 0) then
			counter := 0;
			activated := 0;
			R <= '0';
			R1 <= '0';
			Auto <= "001";
			Fusganger <= "10";
		end if;
		
		if (Mealy = '1' and activated = 0) then
			activated := 1;
			counter := 2;
			Auto <= "010";
			Fusganger <= "10";
		end if;
		
	end process ampe;
end architecture ampel;