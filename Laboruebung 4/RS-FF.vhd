library ieee;
use ieee.std_logic_1164.all;

entity RS_FF is
	port (
		S, R: in std_logic;
		Q: out std_logic
	);
end entity RS_FF;

architecture RS_FlipFlop of RS_FF is
begin
	FlipFlop: process (S, R) is
		variable Q1, Q2: std_logic;
	begin
		Q1 := R nor Q2;
		Q2 := S nor Q1;
		Q <= Q1;
	end process FlipFlop;
end architecture RS_FlipFlop;