library ieee;
use ieee.std_logic_1164.all;

entity RS_FF1 is
	port (
		Q, R1: in std_logic;
		triggered: out std_logic
	);
end entity RS_FF1;

architecture RS_FlipFlop1 of RS_FF1 is
begin
	FlipFlop1: process (Q, R1) is
		variable Q1, Q2: std_logic;
	begin
		Q1 := R1 nor Q2;
		Q2 := Q nor Q1;
		triggered <= Q1;
	end process FlipFlop1;
end architecture RS_FlipFlop1;