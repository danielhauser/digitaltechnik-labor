--
--@author "Mete" Alphan & Daniel Hauser
--@version 0.1
--@description Takes 5 input bits and generates hamming control bits as output
--

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity encoder is
	port(
		A: in std_logic_vector(4 downto 0);
		K: out std_logic_vector(3 downto 0));
end entity encoder;

architecture encoderLogic of encoder is
begin
main: process(A) is
	begin
		K(3)<= A(1)xor A(2) xor A(3) xor A(4);
		K(2)<= A(4)xor A(0) xor A(2);
		K(1)<= A(3)xor A(0) xor A(2);
		K(0)<= A(1);
		
end process main;
end architecture encoderLogic; 
