library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.ALL;

entity decoder is 
	port(	
		D: in std_logic_vector (4 downto 0);
		P: in std_logic_vector (3 downto 0);
		Z: out std_logic_vector(4 downto 0);
		TRANSMISSION_ERROR, CTRL_BIT_ERROR,DEBUG,WORKS: out std_logic
		);
end entity decoder;

architecture decoderLogic of decoder is
signal y: std_logic_vector (3 downto 0);

begin
	-- Generate parity bits with encoder
	Gate0: entity work.encoder(encoderLogic)
			port map (D,y);
	main: process(D,P) is
		variable S: std_logic_vector (3 downto 0);
		variable corrected_output: std_logic_vector (4 downto 0);
		begin							--gedreht
			S(3):= P(3) xor y(3);
			S(2):= P(2) xor y(2);
			S(1):= P(1) xor y(1);
			S(0):= P(0) xor y(0);
			Z<="00000";
			DEBUG <= '0';
			TRANSMISSION_ERROR <= '0';
			CTRL_BIT_ERROR <= '0';
			WORKS<='0';
			if S /= "0000" then
			TRANSMISSION_ERROR <= '1';
					case S is
							when "1000"|"0100"|"0010"|"0001" => --parity-bit fault
									CTRL_BIT_ERROR <= '1';
							when "1100" =>
								Z<="01000";
							when "1010" =>
								Z<="00100";
							when "1110" =>
								Z<="00010";
							when "1001" =>
								Z<="00001";
							when "0110" =>
								Z<="10000";                                             
							when others =>
								DEBUG<= '1';
					end case;
			else 
					WORKS<='1';
			end if;

	end process;
end architecture decoderLogic;
