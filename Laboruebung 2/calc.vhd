--@author Mete Alphan && Daniel Hauser
--@version 0.1
--@description 3-bit calculator with 4 standard calculation functions
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.all;

entity calc is
        port(
                A,B: in std_logic_vector (2 downto 0);
				sel: in std_logic_vector (1 downto 0);
                Z: out std_logic_vector (3 downto 0)
                );
end entity calc;

architecture calcLogic of calc is
signal y0,y1,y2,y3: std_logic_vector(3 downto 0);

begin
	
	Gate0: entity work.threebitAdder(threebitAdderLogic)
		port map (A,B,y0);
	Gate1: entity work.threebitsubtractor(threebitsubtractorLogic)
		port map (A,B,y1);
	Gate2: entity work.threeBitMult(threeBitMultLogic)
		port map (A,B,y2);
	Gate3: entity work.threeBitDivider(threeBitDividerLogic)
		port map (A,B,y3);
	
	main: process(y0,y1,y2,y3) is
		begin 
		if sel = "00" then
			Z <= y0; 			--addierer
		elsif sel = "01" then	
			Z<= y1;				--subtrahierer
		elsif sel = "10" then	
			Z <=y2;				--multiplizierer
		else	
			Z<= y3;			 	--dividierer
		end if;
	end process main;
end architecture calcLogic;