--
--@author Mete Alphan && Daniel Hauser
--@version 0.1
--@description takes a 3-bit (twos complement) vector and multiplies it. 4-Bit-output is in twos complement again
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity threeBitMult is
        port(
                A,B: in std_logic_vector (2 downto 0);
                Z: out std_logic_vector (3 downto 0)
                );
end entity threeBitMult;

architecture threebitMultLogic of threeBitMult is
begin
        main: process(A,B) is
				variable aInt, bInt,mult: integer;
				begin
						aInt:=to_integer(signed(A)); --cast A and B to integers, which allow arithmetical operations
						bInt:=to_integer(signed(B));
						mult:=aInt*bInt; -- result
						if mult>7 then --handle overflow
							Z<=std_logic_vector(to_signed(7,Z'length)); --push "0111"
						elsif mult<-8 then
							Z<=std_logic_vector(to_signed(-8,Z'length)); -- push "1000" 
						else
							Z<=std_logic_vector(to_signed(mult,Z'length));
						end if;
        end process main;
end architecture threeBitMultLogic;