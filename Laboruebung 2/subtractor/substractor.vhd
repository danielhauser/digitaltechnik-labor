--
--@author Mete Alphan && Daniel Hauser
--@version 0.1
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity threebitsubtractor is
        port(
                A,B: in std_logic_vector (2 downto 0);
                Z: out std_logic_vector (3 downto 0)
                );
end entity threebitsubtractor;

architecture threebitsubtractorLogic of threebitsubtractor is
begin
        main: process(A,B) is
				variable aSigned, bSigned: signed (2 downto 0); --(-2)-(3) gibt 0011 (5)
				variable intA,intB: integer;
				begin
                   aSigned:=signed(A);
                   bSigned:=signed(B);
                   --Z<=std_logic_vector(unsigned(resize(aSigned-bSigned,Z'length))); --cast to usigned because signed->st_logic_vector cast implies building of twos complement 
					intA:=to_integer(aSigned);
					intB:=to_integer(bSigned);
					Z<=std_logic_vector(to_signed(intA-intB,Z'length));
		end process main;
end architecture threebitsubtractorLogic;