--
--@author Mete Alphan && Daniel Hauser
--@version 0.1
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity threeBitDivider is
        port(
                A,B: in std_logic_vector (2 downto 0);
                Z: out std_logic_vector (3 downto 0)
                );
end entity threeBitDivider;

architecture threeBitDividerLogic of threeBitDivider is
begin
        main: process(A,B) is
				variable aInt, bInt: integer;
				begin
                        aInt:=to_integer(signed(A));
                        bInt:=to_integer(signed(B));
						
						--check, if bSigned is 0 and set all outputs to '1'
						if bInt = 0 then
							Z<=std_logic_vector(to_unsigned(15));
						else
							Z<=std_logic_vector(to_signed(aInt/bInt, Z'length));
						end if;
        end process main;
end architecture threeBitDividerLogic;