--
--@author Mete Alphan && Daniel Hauser
--@version 0.1
--
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity threebitAdder is
        port(
                A,B: in std_logic_vector (2 downto 0);
                Z: out std_logic_vector (3 downto 0)
                );
end entity threebitAdder;

architecture threebitAdderLogic of threebitAdder is
begin
        main: process(A,B) is
			variable aInt, bInt,mult: integer;
                begin
						aInt:=to_integer(signed(A));
						bInt:=to_integer(signed(B));
							Z<=std_logic_vector(to_signed(aInt+bInt,Z'length));
        end process main;
end architecture threebitAdderLogic;